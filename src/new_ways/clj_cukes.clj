(ns new-ways.clj-cukes
  (:require [clojure.test :refer :all]
            [clojure.string :as str]
            [clojure.zip :as zip]
            [clojure.java.io :as io])
  (:import (java.util.regex Pattern)
           (java.io File)))


(def ^:private stepdef-registry (atom {}))


(defn defstep
  "Add a step definition to the registry"
  [^Pattern pattern step-fn]
  (swap! stepdef-registry assoc (str pattern) {:pattern pattern
                                               :step-fn step-fn}))

(defmacro Given
  "Macro version of defstep with reduced syntax for convenience."
  [^Pattern pattern args-vec & fn-body]
  `(defstep ~pattern (fn ~args-vec ~@fn-body)))

(defmacro When
  "Macro version of defstep with reduced syntax for convenience."
  [^Pattern pattern args-vec & fn-body]
  `(defstep ~pattern (fn ~args-vec ~@fn-body)))

(defmacro Then
  "Macro version of defstep with reduced syntax for convenience."
  [^Pattern pattern args-vec & fn-body]
  `(defstep ~pattern (fn ~args-vec ~@fn-body)))

(defmacro And
  "Macro version of defstep with reduced syntax for convenience."
  [^Pattern pattern args-vec & fn-body]
  `(defstep ~pattern (fn ~args-vec ~@fn-body)))

(defmacro But
  "Macro version of defstep with reduced syntax for convenience."
  [^Pattern pattern args-vec & fn-body]
  `(defstep ~pattern (fn ~args-vec ~@fn-body)))



(defmulti execute-gherkin (fn [state [verb description & body]] verb))

(defn- format-verb
  "Takes a step type (a keyword) and makes it into a nice string for output"
  [verb]
  (-> (str verb)
      (subs 1)
      (str/capitalize)))

(defn splice-background
  "Given a feature body, checks whether it starts with a background and if so
  splices it into each scenario. Otherwise, returns its argument unmodified."
  [[[bg-verb bg-desc & bg-body] & scenarios :as feature-body]]
  (if (= :background bg-verb)
    (for [[verb desc & body] scenarios]
      `[~verb ~desc ~@bg-body ~@body])
    feature-body))

(defmethod execute-gherkin :feature
  [state [verb description & body]]
  (let [expanded-body (splice-background body)]
    (testing (str (format-verb verb) " " description "\n")
      (doall (map (partial execute-gherkin state) expanded-body)))))

(defn find-step [state step]
  "Returns a map containing the step-fn and any matched regex groups.
  Returns nil if no matching stepdef was found"
  (loop [[{:keys [pattern step-fn]} & more] (vals @stepdef-registry)]
    (when pattern
      (if-let [match (re-matches pattern step)]
        (let [groups (when (vector? match) (next match))]
          {:step-fn step-fn :groups groups})
        (recur more)))))

(defn test-step
  "Find the matching step and execute it, passing the state as the first arg"
  [state [step-type step]]
  (let [step-desc (str "  " (format-verb step-type) " " step "\n")
        state     (update state ::steps conj step-desc)]
    (binding [*testing-contexts*
              (concat (::steps state) *testing-contexts*)]
      (let [{:keys [step-fn groups]} (find-step state step)]
        (if step-fn
          (apply step-fn state groups)
          (do-report {:type    :fail
                      :message (str "Could not match step \"" step "\"")}))))))


(defmethod execute-gherkin :scenario
  [state [verb description & body]]
  (testing (str (format-verb verb) " " description "\n")
    (reduce test-step state body)))


(def table-re #"(?<=\|)[^\|]+(?=\|)")

(defn parse-data-table
  "Takes a sequence of strings each representing 1 line of a data table.
  The columns of the table should be surrounded by the pipe character.
  The first row of the table is the header, and for each of the remaining
  rows a map will be returned with values matched to the keys in the header"
  [lines]
  (let [[header & rows] (->> lines
                             (map #(re-seq table-re %))
                             (map #(map str/trim %)))]
    (map #(into {} (map vector header %)) rows)))

;(parse-data-table
;     ["| method | path     | code |"
;      "| GET    | /login   | 200  |"
;      "| POST   | /login   | 200  |"
;      "| GET    | /logout  | 200  |"])

(defmethod execute-gherkin :scenario-outline
  [state [_ description & body]]
  (let [forms (butlast body)
        examples-table (drop 2 (last body))
        examples (parse-data-table examples-table)]
    (doseq [example examples]
      (execute-gherkin
        state
        (concat [:scenario description]
                (for [[verb text] forms]
                  [verb (str/replace text #"<([^>]*)>" #(example (second %)))]))))))


(def keyword-re
  #"(Feature|Background|Scenario(?: Outline)?|Examples|Given|When|Then|And|But):?\s*(.*)")

(defn- keywordize [verb]
  (-> (str/lower-case verb)
      (str/replace #"[_ ]" "-")
      (keyword)))

(defn split-gherkin-line [line]
  (if-let [[_ verb body] (re-matches keyword-re line)]
    [(keywordize verb) body]
    line))

(defn- get-depth [form]
  (case (first form)
    nil 0
    :feature 1
    (:background :scenario :scenario-outline) 2
    :examples 3
    (:given :when :then :and :but) 3
    4))

(defn- apply-times
  "applies f to x the given number of times"
  [f x times]
  (if (<= times 0)
    x
    (recur f (f x) (dec times))))

(defn join-gherkin
  "Adds the new form into the zipper at the end and at the appropriate depth"
  [zipper new-form]
  (let [form      (first zipper)
        depth     (get-depth form)
        new-depth (get-depth new-form)
        new-loc   (apply-times zip/up zipper (inc (- depth new-depth)))]
    (-> (zip/append-child new-loc new-form)
        (zip/down)
        (zip/rightmost))))

(defn parse-gherkin [input]
  (->> (str/split-lines input)
       (map str/trim)
       (remove str/blank?)
       (remove #(str/starts-with? % "#"))
       (map split-gherkin-line)
       (reduce join-gherkin (zip/zipper vector? seq #(vec %2) []))
       (zip/root)))

(defn run-gherkin
  "Runs the input gherkin text, starting with initial-state, which can be nil."
  [initial-state input]
  (doseq [feature (parse-gherkin input)]
    (execute-gherkin initial-state feature)))

(defn run-gherkin-file
  ([path]
   (run-gherkin-file nil path))
  ([initial-state path]
   (run-gherkin initial-state (slurp path))))

(defn run-gherkin-files [directory]
  (doseq [^File file (file-seq (io/file directory))]
    (if (.isFile file)
      (let [path (.toPath file)
            name (.getFileName path)]
        (if (str/ends-with? (str/lower-case name) ".feature")
          (run-gherkin-file file))))))

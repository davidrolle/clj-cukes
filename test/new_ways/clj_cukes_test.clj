(ns new-ways.clj-cukes-test
  (:require [clojure.test :refer :all]
            [new-ways.clj-cukes :refer :all]))

(def gherkin-text
  "Feature: User Login
  Background:
    Given: The app is running
    And: I start a new session

  Scenario: The login page is shown
    When: I browse to the app
    Then: I should see the login page

  Scenario: I can login
    When: I browse to the app
    And: I enter my username in the login field
    And: I enter my password in the password field
    And: I press enter
    Then: I should see the start page")

(def gherkin-data
  [[:feature "User Login"
    [:background ""
     [:given "The app is running"]
     [:and "I start a new session"]]
    [:scenario "The login page is shown"
     [:when "I browse to the app"]
     [:then "I should see the login page"]]
    [:scenario "I can login"
     [:when "I browse to the app"]
     [:and "I enter my username in the login field"]
     [:and "I enter my password in the password field"]
     [:and "I press enter"]
     [:then "I should see the start page"]]]])

(def spliced-background
  '([:scenario "The login page is shown"
     [:given "The app is running"]
     [:and "I start a new session"]
     [:when "I browse to the app"]
     [:then "I should see the login page"]]
    [:scenario "I can login"
     [:given "The app is running"]
     [:and "I start a new session"]
     [:when "I browse to the app"]
     [:and "I enter my username in the login field"]
     [:and "I enter my password in the password field"]
     [:and "I press enter"]
     [:then "I should see the start page"]]))


(deftest parse-gherkin-fn
  (is (= gherkin-data (parse-gherkin gherkin-text))))

(deftest splice-background-fn
  (let [feature-body (-> gherkin-data first rest rest)
        no-background (rest feature-body)]
    (testing "Background is spliced into each scenario"
      (is (= spliced-background (splice-background feature-body))))
    (testing "When no background exists, feature body is unchanged"
      (is (= no-background (splice-background no-background))))))


(def initial-state {:x 1})
(def stateful-gherkin
  "Feature: Test outside state
  Background:
    Given: I assoc :y 2
  Scenario:
    When: I assoc :z 3
    Then: :x should be 1
    And: :y should be 2
    And: :z should be 3")

(Given #"I assoc :(\S+) (\S+)" [state k v]
  (assoc state (keyword k) (read-string v)))

(Then #":(\S+) should be (\S+)" [state k v]
  (is (= (read-string v) ((keyword k) state)))
  state)

(deftest state-handling
  (testing "Test steps update, validate, and return state"
    (is (= {:x 1 :y 2} (-> (test-step {:x 1}
                                      [:when "I assoc :y 2"])
                           (select-keys [:x :y]))))
    (is (= {:x 1 :y 2} (-> (test-step {:x 1 :y 2}
                                      [:then ":y should be 2"])
                           (select-keys [:x :y])))))
  (testing "Scenario passes state to steps"
    (execute-gherkin {:x 1} [:scenario ":x is 1"
                             [:then ":x should be 1"]]))
  (testing "Feature passes state to steps"
    (execute-gherkin {:x 1} [:feature "Test outside state"
                             [:scenario ":x is 1"
                              [:then ":x should be 1"]]]))
  (testing "State can be passed from outside"
    (run-gherkin initial-state stateful-gherkin)))


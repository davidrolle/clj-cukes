(defproject new-ways/clj-cukes "0.2.0"
  :description "A pure Clojure implementation of Cucumber, for behaviour driven development. Early alpha!"
  :url "https://gitlab.com/new-ways/clj-cukes"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]])

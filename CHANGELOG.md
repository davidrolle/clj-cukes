# Change Log
All notable changes to this project will be documented in this file.
This change log follows the conventions of
[keepachangelog.com](http://keepachangelog.com/).

## [Unreleased]

## [0.2.0] - 2018-09-12
### Added
- State can now be passed to any of Gherkin executing functions, so that you
can define an initial state for the tests or pass in options/environment
variables.

### Fixed
- Background steps are now run on every scenario instead of just once
- Re-registering a stepdef with the same pattern now overrides the previous
version (useful during development of your steps at the repl). Previously you
had to reload the whole clj-cukes namespace and start over.

## 0.1.0 - 2018-06-22
### Added
- Parse Gherkin files/strings into Clojure data structures
- Execute Gherkin files/strings or data structures
- Function as well as macro APIs for defining steps
- Support for Feature, Background, Scenario, Scenario Outline, Given, When,
Then, And, But
- Some support for data tables
- Each scenario is run with its own state map that is threaded through the
steps, so that you don't need to use global state atoms.

[Unreleased]: https://gitlab.com/new-ways/clj-cukes/compare/0.1.0...HEAD
[0.2.0]: https://gitlab.com/new-ways/clj-cukes/compare/0.2.0...0.1.0
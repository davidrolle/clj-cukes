# clj-cukes

A pure Clojure implementation of Cucumber, for behaviour driven development.

[![Clojars Project](https://img.shields.io/clojars/v/new-ways/clj-cukes.svg)](https://clojars.org/new-ways/clj-cukes)
[![pipeline status](https://gitlab.com/new-ways/clj-cukes/badges/master/pipeline.svg)](https://gitlab.com/new-ways/clj-cukes/commits/master)

## Rationale

While a Clojure compatible implementation of Cucumber-JVM exists, it is a)
difficult to use, b) poorly documented, and c) does not support a functional
programming style, in the sense that all state must be stored in mutable global
variables making concurrent testing a lot harder than it needs to be.

Therefore we wanted a Cucumber test runner which supports threading of state
through each step, and is easy to use from within Clojure code itself.

This project is still in early Alpha, use with caution! Feel free to open an
issue for any bugs you find or features you'd like added, or contribute your
own changes with a pull or merge request.

## Usage

```clojure
(require '[new-ways.clj-cukes :refer :all])
(require '[clojure.test :refer :all])

;; Define steps with the defstep function, which takes a regex for matching 
;; your gherkin steps to and a function to execute.
;; The function will be passed the current state (a map) as its first argument
;; followed by the matching groups from the regex

(defstep #"^I make a (\S+) request for (\S+)$"
  (fn make-request [state method path]
    (let [method-kw (-> method .toLowerCase keyword)]
      (assoc state ::response ((app) (request method-kw path))))))

;; There are also 5 macros for convenience: Given, When, Then, And, and But
;; These call defstep under the hood, but use a shorter syntax:

(When #"^I make a (\S+) request for (\S+)$" [state method path]
  (let [method-kw (-> method .toLowerCase keyword)]
    (assoc state ::response ((app) (request method-kw path))))))

;; Remember to always return state! Otherwise future steps might fail
(Then #"^The status code should be (\d+)$" [state status-code]
  (is (= (Integer/parseInt status-code) (:status (::response state))))
  state))


;; Add an individual gherkin file to your test suite
(deftest handler
  (run-gherkin-file "test/features/handler.feature"))

;; Or run all the feature files in a folder (recursively)
(run-gherkin-files "test/features")

;; Or simply run some gherkin represented as data
(execute-gherkin
  [:feature "App responds to HTTP requests with the right response code"
   [:scenario "Public routes should receive 200"
    [:when "I make a GET request for /login"]
    [:then "The status code should be 200"]]])

;; All gherkin execution functions can also be passed a state map as their
;; first argument, otherwise an empty map is used as the initial state
(run-gherkin-file {:driver chrome-web-driver} "test/features/ui.feature")

;; You can (and probably should!) run individual steps to make sure they behave
;; as expected
(test-step {::response {:status 404}} 
           [:then "The status code should be 200"])
;; => FAIL in () (:2)
;;      Then The status code should be 200
;;    
;;    expected: (= (Integer/parseInt status-code) (:status (:new-ways.clj-cukes-test/response state)))
;;      actual: (not (= 200 404))

```

## Local Development

- Start a REPL with `clj` or `clj -A:dev` to include the test namespaces
- Run tests once with `clj -A:test:runner`
- Create a pom.xml (just if you need it for the Cursive IDE) with `clj -S:pom`


## Working with Git

We use the Git Flow approach as follows:
1. Master contains the latest stable release
2. Develop contains unreleased but finished features
3. All work is done in feature branches off of develop. A feature should
include tests and update both `README.md` and `CHANGELOG.md` as needed.
4. Release branches are created from develop. The version is updated in
`project.clj` and `CHANGELOG.md` and if needed testing/bugfixing are performed.
The release branch is then merged to both master and develop and given a tag.
5. After a release is tagged it is (currently) manually pushed to Clojars using
`lein deploy clojars`


## License

Copyright © 2018 Nutrition Ivy Ltd.

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
